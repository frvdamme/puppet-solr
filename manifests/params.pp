# @summary Dynamic parameters for the solr class.
#
class solr::params (
){

  # OS Specific configuration
  case $::osfamily {
    'RedHat': {
      $required_packages  = ['java-1.8.0-openjdk','unzip','lsof','wget']
      $java_home          = '/usr/lib/jvm/jre-1.8.0'
      $solr_env           = '/etc/sysconfig/solr'
      if versioncmp($::operatingsystemrelease, '7.0') >= 0 {
        $is_systemd = true
      } else {
        $is_systemd = false
      }
    }
    'debian':{
      $java_home = '/usr/lib/jvm/default-java'
      $solr_env  = '/etc/default/solr'
      if $::operatingsystem == 'Ubuntu' and
      versioncmp($::operatingsystemrelease, '15.04') >= 0 {
        $is_systemd        = true
        $required_packages = ['unzip','lsof','software-properties-common', 'default-jre','wget']
      } else {
        $is_systemd        = false
        $required_packages = ['unzip','lsof','default-jre','wget']
      }
    }
    default: {
      fail("Unsupported OS ${::osfamily}.\
  Please use a debian or redhat based system")
    }
  }
}
